import Franchise from './Franchise';
import Restaurant from './Restaurant';
import Serveur from './Serveur';
import RestaurantGenerator from './utils/RestaurantGenerator';
import ServeurGenerator from './utils/ServeurGenerator';

describe('Chiffre affaire', () => {
    describe('Serveur', () => {
        let serveur: Serveur;

        beforeEach(() => {
            serveur = new Serveur('Test', 500);
        });

        test('Nouveau serveur CA à zéro', () => {
            expect(serveur.ChiffreAffaire).toBe(0);
        });

        test('Nouveau CA du serveur après première commande', () => {
            const montantCommande = 20.5;
            serveur.prendreCommande(montantCommande);
            expect(serveur.ChiffreAffaire).toBe(montantCommande);
        });

        test('CA est la somme des deux commandes', () => {
            const premiereCommande = 15;
            serveur.prendreCommande(premiereCommande);

            const secondeCommande = 20.5;
            serveur.prendreCommande(secondeCommande);

            expect(serveur.ChiffreAffaire).toBe(premiereCommande + secondeCommande);
        });
    });

    describe('Restaurant', () => {
        const serveurGenerator = new ServeurGenerator();

        test('CA du restaurant à deux serveurs prenant la même commande = 2 * montant de la commande', () => {
            const serveurs = Array.from(serveurGenerator.generate(2));

            const restaurant = new Restaurant(serveurs);

            const montantCommande = 20.5;

            serveurs.forEach((serveur) => {
                serveur.prendreCommande(montantCommande);
            });

            expect(restaurant.chiffreAffaire()).toBe(2 * montantCommande);
        });

        test.each([0, 1, 2, 100])(
            'CA du restaurant à X serveurs prenant la même commande = X * montant de la commande',
            (nbServeurs) => {
                const serveurs = Array.from(serveurGenerator.generate(nbServeurs));
                const restaurant = new Restaurant(serveurs);

                const montantCommande = 20.5;

                serveurs.forEach((serveur) => {
                    serveur.prendreCommande(montantCommande);
                });

                expect(restaurant.chiffreAffaire()).toBe(nbServeurs * montantCommande);
            }
        );
    });

    describe('Franchise', () => {
        const restaurantGenerator = new RestaurantGenerator();

        const cases = [
            [0, 0],
            [1, 0],
            [2, 0],
            [1000, 0],
            [0, 1],
            [1, 1],
            [2, 1],
            [1000, 1],
            [0, 2],
            [1, 2],
            [2, 2],
            [1000, 2],
            [0, 1000],
            [1, 1000],
            [2, 1000],
            [1000, 1000],
        ];

        test.each(cases)(
            'CA de la franchise à X restaurants ayant Y serveurs prenant la même commande = X * Y * montant de la commande',
            (x, y) => {
                const restaurants = Array.from(restaurantGenerator.generate(x, y));
                const franchise = new Franchise(restaurants);

                const montantCommande = 20.5;

                restaurants.forEach((restaurant) => {
                    restaurant.Serveurs.forEach((serveur) => {
                        serveur.prendreCommande(montantCommande);
                    });
                });

                expect(franchise.chiffreAffaire()).toBe(x * y * montantCommande);
            }
        );
    });
});
