describe('Début de service', () => {
    // ÉTANT DONNE un restaurant ayant 3 tables
    // QUAND le service commence
    // ALORS elles sont toutes affectées au Maître d'Hôtel
    test('Restaurant avec 3 tables affectées au maître hotel en début de service', () => {
        // let service = new Service();
    });

    // ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur
    // QUAND le service débute
    // ALORS la table éditée est affectée au serveur et les deux autres au maître d'hôtel
    // test('', () => {});

    // ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur
    // QUAND le service débute
    // ALORS il n'est pas possible de modifier le serveur affecté à la table
    // test('', () => {});

    // ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur
    // ET ayant débuté son service
    // QUAND le service se termine
    // ET qu'une table est affectée à un serveur
    // ALORS la table éditée est affectée au serveur et les deux autres au maître d'hôtel
    // test('', () => {});
});
