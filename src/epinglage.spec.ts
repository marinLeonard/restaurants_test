describe('Épinglage', () => {
    // ÉTANT DONNE un serveur ayant pris une commande
    // QUAND il la déclare comme non-payée
    // ALORS cette commande est marquée comme épinglée
    test('', () => {
        const todo = '';
    });

    // ÉTANT DONNE un serveur ayant épinglé une commande
    // QUAND elle date d'il y a au moins 15 jours
    // ALORS cette commande est marquée comme à transmettre gendarmerie
    // test('', () => {});

    // ÉTANT DONNE une commande à transmettre gendarmerie
    // QUAND on consulte la liste des commandes à transmettre du restaurant
    // ALORS elle y figure
    // test('', () => {});

    // ÉTANT DONNE une commande à transmettre gendarmerie
    // QUAND elle est marquée comme transmise à la gendarmerie
    // ALORS elle ne figure plus dans la liste des commandes à transmettre du restaurant
    // test('', () => {});
});
