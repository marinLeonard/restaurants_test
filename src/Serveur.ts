export default class Serveur {
    _chiffreAffaire = 0;

    constructor(private nom: string, private salaire: number) {}

    get ChiffreAffaire() {
        return this._chiffreAffaire;
    }

    private set ChiffreAffaire(val) {
        this._chiffreAffaire = val;
    }

    public prendreCommande(montant: number) {
        this._chiffreAffaire += montant;
    }
}
