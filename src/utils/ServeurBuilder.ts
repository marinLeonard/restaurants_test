import Serveur from '../Serveur';

export default class ServeurBuilder {
    private _nom = '';

    public donnerNom(nom: string) {
        this._nom = nom;
    }

    public build() {
        return new Serveur(this._nom, 0);
    }
}
