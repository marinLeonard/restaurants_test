import ServeurBuilder from './ServeurBuilder';

export default class ServeurGenerator {
    private _builder = new ServeurBuilder();

    public *generate(howMany: number) {
        for (let index = 0; index < howMany; index++) {
            yield this._builder.build();
        }
    }
}
