import Restaurant from '../Restaurant';
import ServeurGenerator from './ServeurGenerator';

export default class RestaurantGenerator {
    private _serveurGenerator = new ServeurGenerator();

    public *generate(howManyRestaurant: number, howManyServeurs: number) {
        for (let index = 0; index < howManyRestaurant; index++) {
            yield new Restaurant(Array.from(this._serveurGenerator.generate(howManyServeurs)));
        }
    }
}
