describe('Menu', () => {
    // ÉTANT DONNE un restaurant ayant le statut de filiale d'une franchise
    // ET une franchise définissant un menu ayant un plat
    // QUAND la franchise modifie le prix du plat
    // ALORS le prix du plat dans le menu du restaurant est celui défini par la franchise
    test('', () => {
        const todo = '';
    });

    // ÉTANT DONNE un restaurant appartenant à une franchise et définissant un menu ayant un plat
    // ET une franchise définissant un menu ayant le même plat
    // QUAND la franchise modifie le prix du plat
    // ALORS le prix du plat dans le menu du restaurant reste inchangé

    // test('', () => {});

    // ÉTANT DONNE un restaurant appartenant à une franchise et définissant un menu ayant un plat
    // QUAND la franchise ajoute un nouveau plat
    // ALORS la carte du restaurant propose le premier plat au prix du restaurant et le second au prix de la franchise
    // test('', () => {});
});
