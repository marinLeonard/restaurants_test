import Restaurant from './Restaurant';

export default class Franchise {
    constructor(private _restaurants: Restaurant[]) {}

    public chiffreAffaire() {
        let chiffreAffaire = 0;

        this._restaurants.forEach((restaurant) => {
            chiffreAffaire += restaurant.chiffreAffaire();
        });

        return chiffreAffaire;
    }
}
