import Serveur from './Serveur';

export default class Restaurant {
    constructor(private _serveurs: Serveur[]) {}

    get Serveurs() {
        return this._serveurs;
    }

    public chiffreAffaire() {
        let chiffreAffaire = 0;

        this._serveurs.forEach((serveur) => {
            chiffreAffaire += serveur.ChiffreAffaire;
        });

        return chiffreAffaire;
    }
}
